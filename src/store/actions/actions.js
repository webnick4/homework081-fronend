import axios from "../../axios-api";
import {FETCH_URL_SUCCESS} from "./actionTypes";


export const fetchUrlSuccess = data => {
  return {type: FETCH_URL_SUCCESS, data};
};

export const getShortUrl = link => {
  return dispatch => {
    return axios.post('/links', link).then(
      response => dispatch(fetchUrlSuccess(response.data))
    )
  }
};