import {FETCH_URL_SUCCESS} from "../actions/actionTypes";

const initialState = {
  link: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_URL_SUCCESS:
      return {...state, link: action.data};
    default:
      return state;
  }
};

export default reducer;