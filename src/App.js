import React, { Component } from 'react';
import Shorters from "./container/Shorters/Shorters";

class App extends Component {
  render() {
    return <Shorters />
  }
}

export default App;
