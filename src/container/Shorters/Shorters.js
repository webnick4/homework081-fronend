import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, ControlLabel, FormControl, FormGroup, HelpBlock} from "react-bootstrap";
import {getShortUrl} from "../../store/actions/actions";
import ShortLink from "../../components/ShortLink/ShortLink";

class Shorters extends Component {
  state = {
    url: '',
    show: false
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.onGetShortUrl({originalUrl: this.state.url});
    this.setState({show: true});
  };


  render() {
    const formStyles = { maxWidth: '70%', margin: '10% auto 10px', textAlign: 'center' };

    function FieldGroup({ id, label, help, ...props }) {
      return (
        <FormGroup controlId={id}>
          <ControlLabel>{label}</ControlLabel>
          <FormControl {...props} />
          {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
      );
    }

    return (
      <Fragment>
        <form
          style={formStyles}
          onSubmit={this.submitFormHandler}
        >
          <h1>Shorten your link</h1>
          <FieldGroup
            id="formControlsText"
            type="text"
            placeholder="Enter URL here"
            name="url"
            value={this.state.url}
            onChange={this.inputChangeHandler}
          />
          <Button type="submit" bsStyle="primary">Shorten</Button>
        </form>

        {this.state.show && <ShortLink shortUrl={this.props.link.shortUrl}/>}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    link: state.link
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetShortUrl: link => dispatch(getShortUrl(link))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Shorters);