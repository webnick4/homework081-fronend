import React from 'react';

const ShortLink = props => {
  const linkStyles = { margin: '40px auto 10px', textAlign: 'center' };

  return (
    <div style={linkStyles}>
      <h3>Your link now looks like this:</h3>
      <a
        href={`http://localhost:8000/${props.shortUrl}`}
      >
        {`http://localhost:8000/${props.shortUrl}`}
      </a>
    </div>
  );
};

export default ShortLink;